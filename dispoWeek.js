var Observable = require("FuseJS/Observable")
var DateTime = require("lib/dateTime")
var moment = require("lib/moment")
var villeOperation = require("lib/villeOperation")
var _ = require("lib/underscore")


var view = this;


var saved = Observable(false)


var week = Observable(new Date())


this.Parameter.onValueChanged(module, function(value){
  week.value = new Date(value.year, value.month, value.day)
})

exports.activated = function(){
  var p = new Date(week.value)
	p.setDate( p.getDate() - 7)
  router.bookmark({
    name: "prevWeek",
    relative: view,
    path: [ "semaine", { month: p.getMonth(), year: p.getFullYear(), day: p.getDate() } ]
  })



  var p = new Date(week.value.valueOf())
	p.setDate( p.getDate() + 7)
  router.bookmark({
    name: "nextWeek",
    relative: view,
    path: [ "semaine", { month: p.getMonth(), year: p.getFullYear(), day: p.getDate() } ]
  })
}


function Day(timeslot, day){
  this.day = day
  this.timeslot = timeslot
}


function Week(label, days) {
  this.label = label;
  this.days = days
}

exports.days = Observable()

week.onValueChanged(module, function(v){
  var all = []
  var week = []
  var startDate = new Date(v)
  var endDate = new Date(startDate.getTime() + 7*24*60*60*1000);

  villeOperation.heureOperation.forEach(function(x){
    for (var iDate = new Date(v); iDate < endDate; iDate.setDate(iDate.getDate() + 1)) {
      all.push(new Day(x, iDate))
    }
  })

  var grouped = _.groupBy(all, 'timeslot')
  for (var variable in grouped) {
    if (grouped.hasOwnProperty(variable)) {
      week.push(new Week(variable, grouped[variable] ))
    }
  }
  exports.days.replaceAll(week)
})


exports.daysOfSemaine = DateTime.dayLabels

exports.weekLabel = week.map(function(v){
  return "Semaine du" + " " + moment(v).startOf('isoweek').get("date") + " " + DateTime.monthLabels[moment(v).startOf('isoweek').get("month")]
})
